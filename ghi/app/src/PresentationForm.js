import { React, useEffect, useState } from 'react'

const PresentationForm = () => {
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState([]);
    const [email, setEmail] = useState([]);
    const [companyName, setCompanyName] = useState([]);
    const [title, setTitle] = useState([]);
    const [synopsis, setSynopsis] = useState([]);
    const [conferenceHref, setConferenceHref] = useState([]);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const handleConferenceHrefChange = (event) => {
        const value = event.target.value;
        setConferenceHref(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.presenter_email = email;
        data.presenter_name = name;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        // data.?? = conferenceHref;

        {/* company_name??? */}
        console.log(data);

        const presentationUrl = `http://localhost:8000${conferenceHref}presentations/`;   // id irrelevant?!?!
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };


        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            console.log(newAttendee);

            setName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConferenceHref('');
        }
    }


    const fetchData = async () => {
        const conferencesUrl = "http://localhost:8000/api/conferences/";

        const conferencesResponse = await fetch (conferencesUrl);
        if (conferencesResponse.ok) {
            const data = await conferencesResponse.json();
            setConferences(data.conferences);
        } else {
            throw new Error('Response not ok!');
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    return (
    <div className="container">
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1 id="conferenceAlert">Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                    <div className="form-floating mb-3">
                    <input value={name} onChange={handleNameChange} placeholder="Name" required name="name" type="text" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input value={email} onChange={handleEmailChange} placeholder="Starts" required name="starts" type="email" id="email" className="form-control" />
                    <label htmlFor="email">Email</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input value={companyName} onChange={handleCompanyNameChange} placeholder="Ends" required name="ends" type="text" id="companyName" className="form-control" />
                    <label htmlFor="companyName">Company Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <textarea value={title} onChange={handleTitleChange} placeholder="Title" required name="title" type="text" id="title" className="form-control"></textarea>
                    <label htmlFor="description">Title</label>
                    </div>
                    <div className="form-floating mb-3">
                    <textarea value={synopsis} onChange={handleSynopsisChange} placeholder="Synopsis" cols="30" rows="10" required name="synopsis" type="text" id="synopsis" className="form-control" />
                    <label htmlFor="synopsis">Synopsis</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleConferenceHrefChange} required name="conference" id="conference" className="form-select">
                        <option value=''>Choose a conference</option>
                        {conferences.map(conference => {
                        return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                        );
                    })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
        </div>
    )
}

export default PresentationForm
