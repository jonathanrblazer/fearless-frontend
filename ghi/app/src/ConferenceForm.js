import { React, useState, useEffect } from 'react'

const ConferenceForm = () => {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [locationID, setLocationID] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const handleLocationIDChange = (event) => {
        console.log(event.target.value);
        const value = event.target.value;
        setLocationID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = locationID;

        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setStarts('');
            setEnds('');
            setMaxPresentations('');
            setMaxAttendees('');
            setDescription('');
            setLocationID('');
        }
    }

    const fetchData = async () => {
        const locationsUrl = "http://localhost:8000/api/locations/";

        const locationsResponse = await fetch (locationsUrl);
        if (locationsResponse.ok) {
            const data = await locationsResponse.json();
            setLocations(data.locations);
        } else {
            throw new Error('Response not ok!');
        }
    }
    useEffect(() => {
        fetchData();
    }, []);





  return (
    <div className="container">
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1 id="conferenceAlert">Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required name="name" type="text" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input value={starts} onChange={handleStartsChange} placeholder="Starts" required name="starts" type="date" id="starts" className="form-control" />
                <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                <input value={ends} onChange={handleEndsChange} placeholder="Ends" required name="ends" type="date" id="ends" className="form-control" />
                <label htmlFor="ends">Ends</label>
                </div>
                <div className="form-floating mb-3">
                <textarea value={description} onChange={handleDescriptionChange} cols="30" rows="10" placeholder="Description" required name="description" type="text" id="name" className="form-control"></textarea>
                <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                <input value={maxPresentations} onChange={handleMaxPresentationsChange} placeholder="Max Presentations" required name="max_presentations" type="number" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="Max Attendees" required name="max_attendees" type="number" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                <select onChange={handleLocationIDChange} required name="location" id="location" className="form-select">
                    <option value=''>Choose a location</option>
                    {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    </div>
  )
}

export default ConferenceForm
