import React from 'react'

const AttendeesList = ({ attendees }) => {
  return (
    <div className="container">
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {attendees.map(attendee => {
          return (
            <tr key={attendee.href}>
              <td>{ attendee.name }</td>
              <td>{ attendee.conference }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </div>
  )
}

export default AttendeesList
