import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <PresentationForm />
    {/* <AttendeeForm /> */}
    {/* <ConferenceForm /> */}
    {/* <LocationForm /> */}
    {/* <AttendeesList
    attendees={props.attendees}/> */}


    </>
  );
}

export default App;
