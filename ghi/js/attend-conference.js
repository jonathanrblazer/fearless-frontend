window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

    const loadingConferenceSpinner = document.getElementById("loading-conference-spinner");
    loadingConferenceSpinner.classList.add("d-none");
    const conferenceTag = document.getElementById("conference");
    conferenceTag.classList.remove("d-none");

    }

    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const attendeeUrl = 'http://localhost:8001/api/attendees/FUDGE';
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newAttendee = await response.json();
        //   console.log(newAttendee);
          if (newAttendee.ok) {
            console.log(newAttendee);
          }
          const attendeeForm = document.getElementById("create-attendee-form");
          attendeeForm.classList.add("d-none");
          const successMessage = document.getElementById("success-message");
          successMessage.classList.remove("d-none");

        } else {
            // throw new Error('attendeeUrl not fetched!');
            const alert = document.querySelector('#makeAlert');
            alert.innerHTML += ` <span class=\'alert alert-warning\'> We couldn't find the attendees! </span> `;
        }
  });

});
