console.log("Hello, Blaze!");


window.addEventListener("DOMContentLoaded", async () => {
    console.log("DOM fully loaded and parsed");


    const statesUrl = "http://localhost:8000/api/states/";

    try {
        const statesResponse = await fetch (statesUrl);
        if (statesResponse.ok) {
            const data = await statesResponse.json();
            const selectTag =  document.getElementById("state");

            for (let state of data.states) {
                const option = document.createElement('option');
                const abbreviation = state.abbreviation;
                const name = state.name;
                option.innerHTML = name;
                option.value = abbreviation;
                selectTag.appendChild(option);
                // console.log(mainTag);
                // console.log(data);
            }
        } else {
            throw new Error('Response not ok');
        }
    } catch (error) {
        console.error('error', error);
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
          console.log(newLocation);

        //   if (newLocation.ok) {
        //     console.log(newLocation);
        // } else {
        //     throw new Error('newLocation not responded!')
        // }
        } else {
            throw new Error('locationUrl not fetched!');
        }
    });




  });
