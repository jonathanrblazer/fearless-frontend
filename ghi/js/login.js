window.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('login-form');
    form.addEventListener('submit', async event => {
      event.preventDefault();

      // const data = Object.fromEntries(new FormData(form))
      // const fetchOptions = {
      //   method: 'post',
      //   body: data,
      //   headers: {
      //     'Content-Type': 'application/json',
      //   }
      // DJWTO gets regular form data, NOT JSON POST requests!!!

    const fetchOptions = {
      method: 'post',
      body: new FormData(form),
      credentials: "include",
          };

      const url = 'http://localhost:8000/login/';
      const response = await fetch(url, fetchOptions);
      if (response.ok) {
        window.location.href = '/';
      } else {
        console.error(response, 'problem with login!');
        const alert = document.querySelector('#makeAlert');
        alert.innerHTML += ` <div class=\'alert alert-warning alert-dismissible\'> Problem with Login! <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>`;
      }
    });
  });
