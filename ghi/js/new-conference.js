console.log("Hello, Blaze!");

/*

*/

window.addEventListener("DOMContentLoaded", async () => {
    console.log("DOM fully loaded and parsed");


    const locationsUrl = "http://localhost:8000/api/locations/";

    try {
        const locationsResponse = await fetch (locationsUrl);
        if (locationsResponse.ok) {
            const data = await locationsResponse.json();
            const selectTag =  document.getElementById("location");

            for (let location of data.locations) {
                const option = document.createElement('option');
                const locationID = location.id;
                const name = location.name;
                option.innerHTML = name;
                option.value = locationID;
                selectTag.appendChild(option);
                // console.log(mainTag);
                // console.log(data);
            }
        } else {
            throw new Error('Response not ok');
        }
    } catch (error) {
        console.error('error', error);
        const alert = document.querySelector('#locationAlert');
        alert.innerHTML += ` <div class=\'alert alert-warning alert-dismissible\'> Problem with Locations! <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>`;

    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newConference = await response.json();
          console.log(newConference);

        //   if (newconference.ok) {
        //     console.log(newConference);
        // } else {
        //     throw new Error('newConference not responded!')
        // }
        } else {
            // throw new Error('conferenceUrl not fetched!');
            console.error(response, 'problem with login!');
            const alert = document.querySelector('#conferenceAlert');
            alert.innerHTML += ` <div class=\'alert alert-warning alert-dismissible\'> Conference not fetched! <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>`;
        }
    });
});
