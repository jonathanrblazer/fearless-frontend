window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl, startLocale, endLocale, locationName) {
        return `
          <div class="col-sm-6 col-lg-4 mb-4">
            <div class="card shadow-lg p-0">
              <img src="${pictureUrl}" class="card-img-top">
              <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <p class="card-text">${description}</p>
              </div>
              <div class="card-footer">
              ${startLocale} - ${endLocale}
            </div>
            </div>
          </div>
        `;
      }

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
    if (!response.ok) {
        // Do something response BAD
        const alert = document.querySelector('#makeAlert');
        alert.innerHTML += ` <span class=\'alert alert-warning\'> We couldn't find the conferences! (response) </span> `;
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {

                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = details.conference.starts;
                const ends = details.conference.ends;
                const startDate = new Date(starts);
                const endDate = new Date(ends);
                const startLocale = startDate.toLocaleDateString();
                const endLocale = endDate.toLocaleDateString();
                const locationName = details.conference.location.name;
                const html = createCard(title, description, pictureUrl, startLocale, endLocale, locationName);
                const row = document.querySelector('.row');
                // const row = document.querySelector('.card-columns');
                row.innerHTML += html;
            }
        }
    }
} catch (e) {
    // ERROR RAISED, DO SOMETHIN
    console.error(e);
    const alert = document.querySelector('#makeAlert');
    alert.innerHTML += ` <span class=\'alert alert-warning\'> We couldn't find the conferences! (catch) `;

  }
});

/* <div class="col"></div>
</div> */
